"""Hard-coded default configuration options (override by command-line args).

Copyright (c) 2023 James Boyden

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

#
# Data-module specific configuration options.
#
batch_size = 16
data_dir = 'dataset/'
resize_image_H_W = (200, 200)
num_workers = 4

#
# Model specific configuration options.
#
num_features_per_scale = [64, 128, 256, 512, 1024]  # same as the U-Net paper
learning_rate = 1.0e-2

#
# Training specific configuration options.
#
min_epochs = 1
num_epochs = 1000

# Overfit batches: Speed-run the training process on just a few batches:
#  https://lightning.ai/docs/pytorch/stable/common/trainer.html#overfit-batches
#
# If you can't overfit your model on just a few batches,
# there might be a problem with your model.
num_overfit_batches = 2  # How many batches to overfit.
do_overfit_batches = False  # Attempt to overfit on just a few batches?

# Short run: Sometimes you want to do an extra-short run,
# just for a quick sanity check.  It won't train anything useful,
# but you can verify that the loss plots are moving downwards.
num_epochs_short = 20
do_short_run = False

#
# Logging specific configuration options.
#
logger_dirname = 'tb_logs'
experiment_name = 'unet_model_v1'

# Log a per-batch image-grid of input images and prediction results.
log_progress_images = False

# Log images every N training epochs.
#
# Note that this is NOT the same as logging every N training steps.
# According to the PyTorch-Lightning docs:
#  https://lightning.ai/docs/pytorch/stable/extensions/logging.html#control-logging-frequency
#   """
#   It may slow down training to log on every single batch.
#   By default, Lightning logs every 50 rows, or 50 training steps.
#   To change this behaviour, set the `log_every_n_steps` `Trainer` flag.
#   """
#
# But of course, there might be multiple steps per training epoch.
# This numerical mismatch might be inconvenient if you wish to observe how the
# prediction results for a particular set of input images improve over time.
#
# Furthermore, the flag `log_every_n_steps` doesn't necessarily behave the way
# some users expect.  (Consult the long comment in function `u_net.train.main`
# for more information about this unexpected behaviour by this flag.)
#
# Hence, we introduce our own logging flag that counts in training epochs.
log_images_every_n_epochs = 50
