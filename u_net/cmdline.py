"""Functions to parse command-line arguments, for use with Python `argparse`.

Copyright (c) 2023 James Boyden

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import argparse
import re


def get_argument_parser(descr):
    repo_url = 'https://gitlab.com/jboyden/u-net-demo/'
    epilog = 'Copyright (C) 2023 James Boyden: %s' % repo_url
    return argparse.ArgumentParser(description=descr, epilog=epilog)


def parse_list(apply_each=None, *, min_len=None, max_len=None):
    """Parse a list of tokens; return an un-instantiated `_ParseListAction`.

    Parse a list of tokens from a single command-line argument string,
    with adjacent tokens separated by either a comma `","` or a space `" "`,
    but NOT both (that is, neither both together nor a mixture of them).

    Examples that will be accepted:

     - `"3"`      # no separator: single-element list
     - `"3 4 5"`  # separators are all space `" "`
     - `"3,4,5"`  # separators are all comma `","`

    Examples that will NOT be accepted:

     - `"3,4 5"`
     - `"3 4,5"`
     - `"3 4,5,"`
     - `"3,4,5,"`
     - `"3 4 5 "`
     - `" 3 4 5"`
     - `","`
     - `", ,"`

    Each/every token in the parsed list may optionally be checked/converted
    by the callable `apply_each` if supplied.

    This callable will be called as: `apply_each.__call__(action, token)`

    An example such `apply_each` callable is function `parse_positive_int`,
    which is also defined in this module.

    The minimum/maximum length of the list may optionally be verified,
    if the keyword-only parameters `min_len` and/or `max_len` are supplied.

    For example, to parse/require a list of tokens of length >= 1,
    then convert each token to a positive integer:

            >>> parser.add_argument('--num-features-per-scale',
            ...         action=parse_list(parse_positive_int, min_len=1))

    As another example, to parse/require a list of tokens of length == 2,
    then convert each token to a positive integer:

            >>> parser.add_argument('--img-height-width',
            ...         action=parse_list(parse_positive_int,
            ...                           min_len=2, max_len=2))
    """

    if apply_each is not None:
        assert hasattr(apply_each, "__call__")
    if min_len is not None:
        assert isinstance(min_len, int)
    if max_len is not None:
        assert isinstance(max_len, int)

    class _ParseListAction(argparse.Action):
        """An `argparse.Action` class to parse a list of tokens.

        This class definition should be returned (but not instantiated!)
        by the function `parse_list` in which this class is defined.

        This class definition is a closure:  When this class is defined,
        it "closes over" the function-call arguments passed to `parse_list`.

        This class definition will be instantiated when it is passed as
        the `action=` argument to `argparse.ArgumentParser.add_argument`.

        Relevant docs about `argparse.Action` and "Action classes":
         https://docs.python.org/3/library/argparse.html#action
         https://docs.python.org/3/library/argparse.html#action-classes
        """
        def __init__(self, option_strings, dest, **kwargs):
            super().__init__(option_strings, dest, **kwargs)
            self.apply_each = (apply_each if apply_each is not None else
                    # Default to an identity transformation.
                    lambda _, val: val)
            self.min_len = min_len
            self.max_len = max_len
            self.list_of_tokens_regex = re.compile(
                    r'^[^, ]+((?P<sep>[, ])[^, ]+((?P=sep)[^, ]+)*)?$')


        def __call__(self, parser, namespace, values, option_string=None):
            parsed_list = self.parse_list(values)
            verified_list = self.verify_list(parsed_list, values)
            setattr(namespace, self.dest, verified_list)


        def parse_list(self, arg_str):
            """Attempt to parse a list of tokens from string `arg_str`.

            If parsing fails, raise exception `argparse.ArgumentError`:
             https://docs.python.org/3/library/argparse.html#exceptions
            """
            if arg_str == "":
                # It's an empty string, so it must be an empty list.
                return []

            m = self.list_of_tokens_regex.match(arg_str)
            if m is None:
                raise argparse.ArgumentError(self,
                        "cannot parse list of tokens: %r" % arg_str)

            # What separator character was used? (if any?)
            sep = m.group('sep')
            if sep is not None:
                # The separator was a comma `","` or a space `" "`.
                return arg_str.split(sep)
            else: # sep is None:
                # There was no separator, so it's only a single-element list.
                return [arg_str]


        def verify_list(self, parsed_list, arg_str):
            """Verify the already-parsed list of tokens in list `parsed_list`.

            If verification fails, raise exception `argparse.ArgumentError`:
             https://docs.python.org/3/library/argparse.html#exceptions
            """
            # Apply `apply_each` to check/convert each element in the list.
            verified_list = [self.apply_each(self, e) for e in parsed_list]

            # Now verify the length of the list.
            list_len = len(verified_list)
            if (min_len is not None) and (list_len < min_len):
                # The list is shorter than the minimum allowed length.
                raise argparse.ArgumentError(self,
                        "len(list) == %d < min_len == %d: %r" %
                                (list_len, min_len, arg_str))

            if (max_len is not None) and (list_len > max_len):
                # The list is longer than the maximum allowed length.
                raise argparse.ArgumentError(self,
                        "len(list) == %d > max_len == %d: %r" %
                                (list_len, max_len, arg_str))

            return verified_list

    return _ParseListAction


def parse_positive_int(action, val):
    """Convert `val` to a positive `int` or raise `argparse.ArgumentError`."""
    try:
        i = int(val)
        if i > 0:
            # It is indeed a positive integer.
            return i
        raise argparse.ArgumentError(action,
                "invalid value for positive integer: %d" % i)
    except (TypeError,ValueError) as e:
        raise argparse.ArgumentError(action,
                "invalid value for positive integer: %r" % val)
