"""A Lightning module that implements a U-Net model for image segmentation.

Python classes (Lightning modules) defined within this Python module:

- :class:`U_Net`

Copyright (c) 2023 James Boyden

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import torch
import torchmetrics
import pytorch_lightning as pl

from torch import argmax as torch_argmax
from torch import cat as torch_cat
from torch import float32 as torch_float32
from torch import int64 as torch_int64
from torch import nn, optim

from typing import Sequence

import conf

# Submodule `argcheck` provides automated checking of function-call argument
# types & values via the `@validate_call` decorator.
#
# The constraints upon argument types/values are specified per-parameter
# using PEP-484 type hint annotations (``parameter_name: type_annotation``),
# extended using the PEP-593 ``Annotated[T, metadata]`` type annotation
# for value constraints as metadata.
#
# If an argument check is violated when the decorated function is called,
# an exception of type `argcheck.ArgCheckException` will be raised.
# For more details, consult the docstrings in the `argcheck` submodule.
from argcheck import Annotated, ArgCheckException
from argcheck import isMonotonicIncr, isNotEmpty, isPositive, eachAll
from argcheck import validate_call

from cmdline import parse_list, parse_positive_int
from submodules import U_NetDoubleConv


class U_Net(pl.LightningModule):
    """This Lightning module implements a U-Net model.

    In this implementation of a U-Net model, we made 5 significant deviations
    from the original U-Net paper:

    1. In contrast to the fixed input resolution of 572 x 572 pixels specified
      in the original U-Net paper, this implementation accepts images of an
      arbitrary (height, width) input resolution.

    2. In contrast to the fixed sequence of num-features-per-level specified
      in the original U-Net paper (specifically: [64, 128, 256, 512, 1024]),
      this implementation allows client code to specify any number of levels,
      and any number of features per level.

    3. In submodule :class:`U_NetDoubleConv`:
      Rather than the 3x3 "valid convolutions" that shave 1px off the border
      of the convolution result (resulting in a progressive reduction of the
      (height, width) resolution of each successive feature map), we instead
      use 1px-padded 3x3 "same convolutions" that maintain the resolution.
      This avoids the need to crop "skip connections" before concatenation;
      and also avoids the need to tile multiple less-than-input-image-size
      segmentation results.

    4. In submodule :class:`U_NetDoubleConv`:
      We apply batch normalisation after the 3x3 convolution, before the ReLU.
      [And then because of the batch normalisation step, there's no benefit
      to adding a bias after the convolution step, because any bias would be
      negated by the batch normalisation anyway.  So we specify ``bias=False``
      to the ``nn.Conv2d`` constructor.]

    5. Rather than exactly 2 channels of output by the final convolution layer
      (for a per-pixel multiclass classifier of exactly 2 classes), the final
      convolution layer in this implementation allows any positive number of
      output channels (for any number of output classes), even including just
      1 channel (for a per-pixel binary classifier).
    """

    @staticmethod
    def add_model_specific_args(parser):
        # Hyperparameters for the U-Net model implementation.
        group = parser.add_argument_group('model',
                'Hyperparameters for the U-Net model:')

        # Note: The `U_Net` hyperparameters `in_channels` & `out_channels`
        # must correspond to the number of image channels in the dataset's
        # training images, and the number of classes in the dataset
        # (because that's what the model will be trained upon).
        #
        # So there's no point allowing the caller to specify
        # these hyperparameters as command-line arguments.
        group.add_argument('--num-features-per-scale', metavar='int,int,...',
                action=parse_list(parse_positive_int, min_len=1),
                default=conf.num_features_per_scale)
        group.add_argument('--learning-rate', metavar='float', type=float,
                default=conf.learning_rate)

        return parser


    @validate_call
    def __init__(self,
            in_channels: Annotated[int, isPositive]=3,
            out_channels: Annotated[int, isPositive]=2,
            num_features_per_scale: Annotated[
                    Sequence[int],
                    [isNotEmpty, isMonotonicIncr, eachAll(isPositive)]
            ]=[64, 128, 256, 512, 1024],
            *,
            learning_rate: Annotated[float, isPositive]=conf.learning_rate,

            # Quoting section 3 ("Training") of the U-Net paper:
            #   "The energy function is computed by a pixel-wise soft-max
            #   over the final feature map combined with the cross entropy
            #   loss function."
            #
            # Note that Torch's `nn.CrossEntropyLoss` assumes logits
            # ("unnormalized logits for each class") as the loss input:
            #  https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html
            #
            # Also from the docs for `nn.CrossEntropyLoss`:
            #   """
            #   The target that this criterion expects should contain either:
            #
            #     - Class indices in the range `[0, C)` where `C` is the number
            #       of classes; if `ignore_index` is specified, this loss also
            #       accepts this class index (this index may not necessarily be
            #       in the class range).
            #   """
            # and:
            #   """
            #   The performance of this criterion is generally better
            #   when `target` contains class indices, as this allows
            #   for optimized computation. Consider providing `target`
            #   as class probabilities only when a single class label
            #   per minibatch item is too restrictive.
            #   """
            multi_loss_fn=nn.CrossEntropyLoss,

            # For binary classification, we use Torch's `nn.BCEWithLogitsLoss`:
            #  https://pytorch.org/docs/stable/generated/torch.nn.BCEWithLogitsLoss.html
            # which expects logits as input (similar to `nn.CrossEntropyLoss`,
            # which we're using for multiclass classification), in contrast to
            # `nn.BCELoss.html`, which expects input probabilities in (0,1):
            #  https://pytorch.org/docs/stable/generated/torch.nn.BCELoss.html
            binary_loss_fn=nn.BCEWithLogitsLoss,
    ):
        """Construct a U-Net with specified architectural hyperparameters.

        Args:
            in_channels (positive int):
                defaults to ``3``, expecting an RGB image
            out_channels (positive int):
                defaults to ``2``, for 2-class multiclass classification
            num_features_per_scale (non-empty sequence of positive int):
                defaults to ``[64, 128, 256, 512, 1024]``, like the U-Net paper
            learning_rate (float):
                defaults to `1.0e-2`
            multi_loss_fn (loss function to use for multi-class):
                defaults to ``nn.CrossEntropyLoss``
            binary_loss_fn (loss function to use for binary-class):
                defaults to ``nn.BCEWithLogitsLoss``

        `num_features_per_scale` is a non-empty sequence of positive integers,
        one element per level of the U-Net.  The integer for each level is the
        desired number of features to exist by the end of that level, computed
        by the 2 blue-arrow convolutions within that level.
          
        The default list argument corresponds to the architecture illustrated
        in the original U-Net paper.

        [Note: In the architecture illustrated in the original U-Net paper,
        the number of features at each level doubles at the first rightward
        blue arrow on that level of the contracting path (left side);
        and then halves again at the first rightward blue arrow on that level
        of the expanding path (right side).
          
        But strictly speaking, this doubling/halving of features is simply a
        design choice in the original U-Net paper:  It's not required by any
        innate mathematical characteristic of the network or the convolutions
        (which are, after all, simply matrix multiplications from an "input"
        number of dimensions to an "output" number of dimensions).

        What *is* required by the network:

          - (1) The **number of levels** on the contracting path (left side)
            **equals** the number of levels on the expanding path (right side),
            so that the skip connections match up.
          - (2) The **image resolution halves** from level to level
            on the contracting path due to the 2x2 max-pooling.
          - (3) The **image resolution doubles** from level to level
            on the expanding path due to 2x2 transposed convolutions.

        This U-Net implementation assumes requirements (1), (2), and (3);
        and implements accordingly.

        [And, although *also* not strictly necessary, this implementation also
        assumes that the **number of features** at each level **matches** on
        the left & right sides of the "U" shape.  It's not strictly necessary
        because the skip connections in the U-Net only require feature-matrix
        concatenation, rather than any element-wise feature-matrix operations.]

        This implementation allows client code to specify any number of levels,
        and any number of features at each level.
        """
        super().__init__()
        # Save all hyperparameters for easy re-loading:
        #  https://lightning.ai/docs/pytorch/stable/common/checkpointing_basic.html#save-hyperparameters
        #
        # Fun fact: PyTorch-Lightning actually defines "hyperparameters"
        # as the arguments to a model's `__init__` method.
        #
        # So the hyperparameters that will be saved by the following
        # method-call are the arguments to this `__init__` method.
        # The hyperparametrs will be saved in some `*/*/*/hparams.yaml` file.
        self.save_hyperparameters()

        # Set up the appropriate loss function.
        self.loss_fn = (
                # multi-class classification:
                multi_loss_fn if out_channels >= 2
                # else, binary classification:
                else binary_loss_fn)

        if isinstance(self.loss_fn, type):
            # It's an unconstructed type.  So construct it!
            self.loss_fn = self.loss_fn()

        # Set up the Learning Rate.
        self.learning_rate = learning_rate

        # Install functions to log the accuracy & f1-score.
        self.num_classes = (out_channels if out_channels >= 2 else 2)
        # To calculate accuracy, we use TorchMetrics' `MulticlassAccuracy`:
        #  https://torchmetrics.readthedocs.io/en/stable/classification/accuracy.html#torchmetrics.classification.MulticlassAccuracy
        # which takes a tensor of targets and a tensor of predictions:
        #   """
        #     - `preds` (`Tensor`): An int tensor of shape `(N, ...)`
        #       or float tensor of shape `(N, C, ..)`.
        #       If preds is a floating point we apply `torch.argmax`
        #       along the `C` dimension to automatically convert
        #       probabilities/logits into an int tensor.
        #
        #     - `target` (`Tensor`): An int tensor of shape `(N, ...)`
        #   """
        self.accuracy = torchmetrics.Accuracy(
                task="multiclass",
                num_classes=self.num_classes,
        )
        #self.f1_score = torchmetrics.F1Score(
        #        task="multiclass",
        #        num_classes=self.num_classes,
        #)

        # Construct the layers in the U-Net architecture.
        self._construct_architecture(in_channels, out_channels,
            num_features_per_scale)

        # This determines whether input images & prediction-result images
        # will be saved in per-batch image-grids in method `training_step`,
        # and the interval of training epochs at which to save the images:
        #
        # This initial value of `None` means that image-logging is disabled.
        # Set a positive integer value in function `u_net.train.main()` to
        # activate logging of progress images.
        self.log_images_every_n_epochs = None

        self.prediction_renderer = None


    def _construct_architecture(self, in_channels, out_channels,
            num_features_per_scale):
        # What's the difference between `nn.ModuleList` and `nn.Sequential`?
        #
        # `nn.Sequential` is a chained sequence of modules, encapsulated in a
        # module that, when instructed to process itself (e.g. with `forward`),
        # processes the modules in the sequence, chaining outputs to inputs.
        #
        # In contrast, `nn.ModuleList` is simply a container of modules
        # (in which the contained modules are all properly registered, as if
        # they were each contained directly as an attribute of the class);
        # but the modules are not connected for processing.
        #
        # More info in the docs:
        #  https://pytorch.org/docs/stable/generated/torch.nn.Sequential.html
        #  https://pytorch.org/docs/stable/generated/torch.nn.ModuleList.html
        self.contracting_path_convs = nn.ModuleList()
        self.exp_path_blue_arrows = nn.ModuleList()
        self.exp_path_green_arrows = nn.ModuleList()

        # This `nn.MaxPool2d(kernel_size=2, stride=2)` halves the resolution
        # in each image dimension of (rows, columns) of pixels, to step from
        # one U-Net level down to the level below.
        #
        # Note that we only need to instantiate a single `MaxPool2d` instance,
        # which will be used for *all* max-pooling within this module, because
        # `MaxPool2d` is non-parametric (non-learning) and may thus be re-used
        # any number of times.
        #
        # Note also that this `stride=2` max-pooling operation will perform
        # a flooring integer-division-by-2 in the (height, width) dimensions
        # of the image resolution.  So the output image resolution from this
        # max-pooling operation will be "half or less" the input resolution
        # in each dimension.  So doubling the resolution of the output image
        # afterwards is NOT guaranteed to produce a resolution equal to that
        # of the input image.
        self.downscale_pool = nn.MaxPool2d(kernel_size=2, stride=2)

        # In this loop, we instatiate the double-blue arrows at each scale
        # on the contracting path (left side) of the architecture diagram in
        # the original U-Net paper.
        # 
        # Note that we're not *connecting* these modules together in this loop:
        # We're simply creating them and storing them in a list for later use
        # in the `forward` method.
        for num_features in num_features_per_scale[:-1]:
            self.contracting_path_convs.append(
                    U_NetDoubleConv(in_channels, num_features))
            in_channels = num_features

        # This is the bottom-most level along the "base" of the U-Net.
        # We store this `U_NetDoubleConv` as its own attribute, separately
        # from `self.contracting_path_convs`, because in the `forward` method,
        # this "base" level does not produce "skip connections"; nor is its
        # result downscaled by max-pool.
        #
        # Because `num_features_per_scale` is guaranteed to be non-empty,
        # `num_features_per_scale[-1]` is guaranteed to be a valid index.
        self.base_level_of_unet = \
                U_NetDoubleConv(in_channels, num_features_per_scale[-1])

        # In this loop, we instantiate the expanding path (right side)
        # of the architecture diagram in the original U-Net paper:
        # both the rightward-pointing double-blue arrows at each scale
        # and the upward-pointing green arrows between scales (levels).
        #
        # We construct from the base level of the "U" shape,
        # upwards and to the right (in the order that processing will occur);
        # but again, note that we're not *connecting* these modules together
        # for processing in this loop.
        #
        # For example, if:
        #
        #   >>> num_per_scale = [64, 128, 256, 512, 1024]
        #
        # then:
        #
        #   >>> num_per_scale[1:] == [128, 256, 512, 1024]
        #   True
        #   >>> num_per_scale[:-1] == [64, 128, 256, 512]
        #   True
        #   >>> list(zip(num_per_scale[1:], num_per_scale[:-1]))
        #   [(128, 64), (256, 128), (512, 256), (1024, 512)]
        #   >>> list(reversed(list(zip(num_per_scale[1:], num_per_scale[:-1]))))
        #   [(1024, 512), (512, 256), (256, 128), (128, 64)]
        #
        # This awkward code is because `zip` objects are not reversible.
        for (num_features_before, num_features_after) in reversed(list(zip(
                num_features_per_scale[1:],  # features before RHS upscale
                num_features_per_scale[:-1]  # features after RHS upscale
        ))):
            # First, the up-pointing green arrows for upscaling between levels.
            #
            # The first argument is `num_features*2` because we're upscaling
            # from a higher-feature-count lower scale to a lower-feature-count
            # higher scale on the right side (expanding path) of the U-Net.
            assert num_features_before > num_features_after
            self.exp_path_green_arrows.append(
                    nn.ConvTranspose2d(
                            num_features_before, num_features_after,
                            kernel_size=2, stride=2,
                    )
            )
            # Now we use `(num_features_after * 2)` because we've concatenated
            # the feature-matrix of the skip connection.
            #
            # [It may be that `(num_features_after * 2 == num_features_before)`
            # (if the specified number of features doubles from level to level
            # on the contracting path, as in the original U-Net paper),
            # but it's not strictly required.]
            self.exp_path_blue_arrows.append(
                    U_NetDoubleConv(num_features_after * 2, num_features_after))

        # Verify that before each double-blue arrow on the expanding path,
        # there is an up-pointing green arrow for upscaling image resolution
        # between levels.
        #
        # [This is an implementation-correctness check, not an argument check.]
        assert len(self.exp_path_green_arrows) == len(self.exp_path_blue_arrows)

        # Now we can combine the matching (green arrows, double-blue arrows)
        # into the "expanding path", stored as a `list` for repeat iteration:
        self.expanding_path_convs = list(zip(
                self.exp_path_green_arrows,
                self.exp_path_blue_arrows))

        # Verify that the number of levels on the contracting path equals
        # the number of levels on the expanding path.
        #
        # [This is an implementation-correctness check, not an argument check.]
        assert len(self.contracting_path_convs) == len(self.expanding_path_convs)
        # Now a final 1x1 conv to produce the per-pixel classification logits.
        # It doesn't change the resolution (height, width) of the layers at the
        # top of the right side (expanding path); it only changes the number of
        # features so that we have a classifier at the end of the network.
        #
        # Because `num_features_per_scale` is guaranteed to be non-empty,
        # `num_features_per_scale[0]` is guaranteed to be a valid index.
        self.final_conv = nn.Conv2d(
                num_features_per_scale[0], out_channels,
                kernel_size=1)


    def forward(self, x):
        # We'll store the corresponding per-level skip connections.
        skip_connections = []

        # Down the contracting path (left side of the "U" shape).
        # The contracting path downscales the image resolution and
        # increases the number of features.
        for blue_arrows_to_increase in self.contracting_path_convs:
            # Increase number of features, maintain input image resolution:
            x = blue_arrows_to_increase(x)
            skip_connections.append(x)
            # The downscaling red arrow at the end of the current level.
            # Downscale image resolution:
            x = self.downscale_pool(x)

        # Across the "base" level of the U-Net.
        x = self.base_level_of_unet(x)

        # Reverse the corresponding per-level skip connections
        # (which were appended in order of decreasing resolution)
        # to be now in order of increasing resolution.
        skip_connections = skip_connections[::-1]

        # Now back up the expanding path (right side of the "U" shape).
        for (skip_conn, (green_arrow_to_upscale, blue_arrows_to_reduce)) \
                in zip(skip_connections, self.expanding_path_convs):

            x = green_arrow_to_upscale(x)

            # NOTE: Even using 1px-padded 3x3 "same convolutions" that maintain
            # the same image resolution, the computed `skip_conn` at this level
            # is NOT guaranteed to have the same resolution as the upscaled `x`
            # at this level, due to the flooring integer-division-by-2 in each
            # dimension by the max-pooling operation `self.downscale_pool(x)`.
            #
            # [For more information, consult the comment before the definition
            # of `self.downscale_pool` in the `__init__` method.]
            #
            # Torch function `torch.cat` states:
            #  https://pytorch.org/docs/stable/generated/torch.cat.html
            #   """
            #   All tensors must either have the same shape
            #   (except in the concatenating dimension) or be empty.
            #   """
            #
            # So before we attempt to concatenate `skip_conn` and `x`, we must
            # ensure that they have the same shape.
            if x.shape != skip_conn.shape:
                # Oh no, our worst fears [see footnote] have been confirmed!
                # 
                # [footnote] "Our worst fears" that occur with >=75% probability
                # for images of an arbitrary (height, width) input resolution.
                #
                # However will we handle this entirely unforseen (>=75%) event??
                #
                # First, we assume that some dimension of `x` is smaller
                # than the corresponding dimension of `skip_conn`, due to
                # a flooring integer-division-by-2 max-pooling downscale
                # (after `skip_conn` was produced) in the contracting path.
                #
                # Secondly, we assume that if we've repaired this problem as
                # we've encountered it at each level, the shape discrepancy
                # will not be more than 1px in each dimension.
                #
                # Let's first verify these assumptions to ensure we're on
                # the right track.  Assume a U-Net input shape:
                #   `(batch_size, img_channels, img_height, img_width)`
                # that becomes an internal feature-map shape:
                #   `(batch_size, num_features, img_height, img_width)`.
                # We want to extract the (height, width) for comparison.
                (_sk_bsize, _sk_nfeats, sk_height, sk_width) = skip_conn.shape
                ( _x_bsize,  _x_nfeats,  x_height,  x_width) = x.shape
                # Why are we in this if-statement block?
                # Verify that at least one of the dimensions of `x` is smaller.
                x_height_needs_padding = (sk_height > x_height)
                x_width_needs_padding = (sk_width > x_width)
                assert (x_height_needs_padding or x_width_needs_padding)
                # We've verified that there's a shape discrepancy.
                # Verify that the shape discrepancy is not more than 1px.
                assert (sk_height == x_height or
                        sk_height == x_height + 1)
                assert (sk_width == x_width or
                        sk_width == x_width + 1)

                # Now,
                # 1. Do we want to pad `x` to the shape of `skip_conn`?
                # 2. Do we want to raise an exception and blame the client code
                #    that supplied an us input image that was not a power-of-2
                #    in its input (height, width) resolution?
                # 3. Do we want to image-resize `x` to have the same shape
                #    as `skip_conn` (eg, using bilinear interpolation)?
                #
                # Option #3 sounds expensive to compute (especially over
                # so many feature channels).  Option #2 seems a little unkind
                # towards our mathematically-challenged client, who just wants
                # to segment their image please.
                #
                # So, Option #1 it is...  I think it's reasonable to pad `x`
                # with an extra row and/or extra column of pixels as needed,
                # by duplicating the current boundary pixels.
                #
                # Normally in this situation, I'd use `numpy.empty_like`,
                # then initialise the memory manually using ndarray slicing.
                # But I want to maintain the Pytorch autograd graph.
                # So instead, I'll use `torch.cat`.
                if x_height_needs_padding:
                    # eg, if `(sk_height == 5)` but `(x_height == 4)`,
                    # we want to duplicate the row at `[x_height=3]`.
                    # To maintain the same `ndim` (number of dimensions),
                    # we'll use  `[x_height - 1:]`.
                    # The following slicing assumes a shape of:
                    #   `(batch_size, num_features, width, width)`.
                    padding_for_x_height = x[:, :, x_height - 1:, :]
                    # Use `dim=2` to concatenate along the "height" dimension.
                    x = torch_cat((x, padding_for_x_height), dim=2)

                if x_width_needs_padding:
                    # eg, if `(sk_width == 9)` but `(x_width == 8)`,
                    # we want to duplicate the row at `[x_width=7]`.
                    # To maintain the same `ndim` (number of dimensions),
                    # we'll use  `[x_width - 1:]`.
                    # The following slicing assumes a shape of:
                    #   `(batch_size, num_features, width, width)`.
                    padding_for_x_width = x[:, :, :, x_width - 1:]
                    # Use `dim=3` to concatenate along the "width" dimension.
                    x = torch_cat((x, padding_for_x_width), dim=3)

            # Now we can be confident we can concatenate `skip_conn` and `x`.
            # Use `dim=1` to concatenate along the "features" dimension.
            skip_concat_x = torch_cat((skip_conn, x), dim=1)
            x = blue_arrows_to_reduce(skip_concat_x)

        # Now a final 1x1 conv to produce the per-pixel classification logits.
        return self.final_conv(x)


    def _calc_one_step_loss(self, batch, batch_idx, *, calc_preds=False):
        (x, targets) = batch
        #print("\nbatch_idx = %d" % batch_idx)

        #print("x.dtype == %s" % x.dtype)
        # => "x.dtype == torch.float32"
        #print("x.shape == %s" % str(x.shape))
        # => "x.shape == torch.Size([16, 3, 200, 200])"
        #print("x.{min,mean,max} == {%.2f, %.2f, %.2f}" %
        #        (x.min(), x.mean(), x.max()))
        # => "x.{min,mean,max} == {0.00, 0.43, 1.00}"

        assert x.dtype == torch_float32
        assert x.ndim == 4

        #print("targets.dtype == %s" % targets.dtype)
        # => "targets.dtype == torch.float32"
        #print("targets.shape == %s" % str(targets.shape))
        # => "targets.shape == torch.Size([16, 21, 200, 200])"
        #print("targets.{min,mean,max} == {%.2f, %.2f, %.2f}" %
        #        (targets.min(), targets.mean(), targets.max()))
        # => "targets.{min,mean,max} == {0.00, 0.05, 1.00}"

        assert targets.dtype == torch_float32
        assert targets.ndim == 4
        (B, C, H, W) = targets.shape

        assert x.shape[0] == B
        assert x.shape[2] == H
        assert x.shape[3] == W

        logits = self.forward(x)  # equivalent to: logits = self(x)

        #print("logits.dtype == %s" % logits.dtype)
        # => "logits.dtype == torch.float32"
        #print("logits.shape == %s" % str(logits.shape))
        # => "logits.shape == torch.Size([16, 21, 200, 200])"
        #print("logits.{min,mean,max} == {%.2f, %.2f, %.2f}" %
        #        (logits.min(), logits.mean(), logits.max()))
        # => "logits.{min,mean,max} == {-15.30, -1.90, 4.23}"

        assert logits.shape == targets.shape
        assert logits.dtype == targets.dtype

        # NOTE: `(targets.dtype == torch.float32)` because in staticmethod
        # function `PascalVOCSearchDataset._convert_to_segmentation_masks()`
        # (in module `u_net.dataset_albument_vocseg`), each PNG colour image
        # of `SegmentationClass` semantic labels is converted into a single
        # multi-channel boolean array of dtype `torch.float32` (which is the
        # mask format expected/required by the Albumentations library).
        #
        # But as we can read in the documentation above:
        #
        #  - for our loss function, `multi_loss_fn = nn.CrossEntropyLoss`,
        #    the loss criterion performs better when `targets` contains
        #    integer class indices.
        #  - for our accuracy function, `accuracy = MulticlassAccuracy`,
        #    the target is expected to be an int tensor of class indices.
        #
        # So, let's convert our targets into integer class indices.
        targets = torch_argmax(targets, dim=1)
        #print("\ntargets = torch_argmax(targets, dim=1)")
        #print("targets.shape == %s" % str(targets.shape))
        # => "targets.shape == torch.Size([16, 200, 200])"
        #print("targets.dtype == %s" % targets.dtype)
        # => "targets.dtype == torch.int64"

        assert targets.dtype == torch_int64
        assert targets.ndim == 3
        assert targets.shape == (B, H, W)

        #loss = F.cross_entropy(logits, targets)
        loss = self.loss_fn(logits, targets)
        #print("loss == %.4f" % loss)

        if calc_preds:
            # Convert logits into predictions (integer class indices).
            preds = torch_argmax(logits, dim=1)
            assert preds.dtype == torch_int64
            assert preds.ndim == 3
            assert preds.shape == (B, H, W)

            #print("type(preds) == %s" % type(preds))
            # => "type(preds) == <class 'torch.Tensor'>"
            #print("preds.dtype == %s" % preds.dtype)
            # => "preds.dtype == torch.int64"
            #print("preds.{min,mean,max} == {%.2f, %.2f, %.2f}" %
            #        (preds.min(), preds.to(torch_float32).mean(), preds.max()))
            # => "preds.{min,mean,max} == {0.00, 0.16, 8.00}"

            return (x, targets, logits, preds, loss)

        else:
            return (x, targets, logits, loss)


    def training_step(self, batch, batch_idx):
        # Calculate whether we should imsave predictions in this epoch.
        # (If we *should* imsave predictions, then we must additionally
        # calculate the predictions.)
        do_imsave_predictions = False
        log_images_every_n_epochs = self.log_images_every_n_epochs
        if log_images_every_n_epochs is not None:
            assert (log_images_every_n_epochs > 0)
            do_imsave_predictions = (0 ==
                    (self.trainer.current_epoch % log_images_every_n_epochs))

        if do_imsave_predictions:
            # Yes; so also calculate the predictions.
            (x, targets, logits, preds, loss) = \
                    self._calc_one_step_loss(batch, batch_idx, calc_preds=True)
            self._imsave_predictions(batch_idx, x, targets, preds)
        else:
            (x, targets, logits, loss) = \
                    self._calc_one_step_loss(batch, batch_idx)

        accuracy = self.accuracy(logits, targets)
        #print("accuracy == %.4f" % accuracy)
        #f1_score = self.f1_score(logits, targets)
        self.log_dict({
                        'train_loss': loss,
                        'train_acc': accuracy,
                        #'train_f1': f1_score
                },
                on_step=False, on_epoch=True, prog_bar=True)
        return loss


    def _imsave_predictions(self, batch_idx, images, targets, preds):
        assert (self.prediction_renderer is not None)

        logger = self.logger.experiment
        ident = 'e%04d_b%d' % (self.trainer.current_epoch, batch_idx)

        rendered_images = self.prediction_renderer(images, targets, preds)
        for name, rendered in rendered_images:
            logger.add_image("%s_%s" % (ident, name), rendered)


    def validation_step(self, batch, batch_idx):
        (x, targets, logits, loss) = self._calc_one_step_loss(batch, batch_idx)
        self.log('val_loss', loss)
        return loss


    def test_step(self, batch, batch_idx):
        (x, targets, logits, loss) = self._calc_one_step_loss(batch, batch_idx)
        self.log('test_loss', loss)
        return loss


    def predict_step(self, batch, batch_idx):
        (x, targets) = batch
        logits = self.forward(x)  # equivalent to: logits = self(x)
        preds = torch_argmax(logits, dim=1)
        return preds


    def configure_optimizers(self):
        # This is also where we would use a learning-rate scheduler.
        return optim.Adam(self.parameters(), lr=self.learning_rate)

