"""A Lightning data-module for the PASCAL VOC segmentation dataset.

Python classes (Lightning data-modules) defined within this Python module:

- :class:`PascalVocSegDataModule`

Copyright (c) 2023 James Boyden

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import os
import torch
import pytorch_lightning as pl

from torch import float32 as torch_float32
from torch import int64 as torch_int64
from torch import uint8 as torch_uint8
from torch import moveaxis as torch_moveaxis

from torch.utils.data import DataLoader
#from torch.utils.data import random_split

# Base class `PascalVOCSearchDataset` expects transforms from Albumentations,
# rather than transforms from TorchVision.  And there are some transforms that
# are expected/needed by the training process:
#  - resize all input images to the same (height, width) for batching
#  - normalize image channels
#  - convert images to tensors
# or else errors will occur during training.  (Ask me how I know this!)
#
# For example, if your input images and masks are not of the same type
# (for example, if one of them has been transformed but the other hasn't),
# Torch function `F.conv2d` will raise an exception like this:
#
#   RuntimeError: expected scalar type Byte but found Float
#
# And because the images in PASCAL VOC are all different shapes, they will be
# loaded as tensors of different shapes; and then when PyTorch tries to stack
# a batch of input image tensors into a single batched tensor, the following
# exception will be raised:
#
#   RuntimeError: Caught RuntimeError in DataLoader worker process 0.
#   Original Traceback (most recent call last):
#     File "/usr/local/lib/python3.10/dist-packages/torch/utils/data/_utils/worker.py", line 308, in _worker_loop
#       data = fetcher.fetch(index)
#     File "/usr/local/lib/python3.10/dist-packages/torch/utils/data/_utils/fetch.py", line 54, in fetch
#       return self.collate_fn(data)
#     File "/usr/local/lib/python3.10/dist-packages/torch/utils/data/_utils/collate.py", line 265, in default_collate
#       return collate(batch, collate_fn_map=default_collate_fn_map)
#
#   <snip...>
#
#     File "/usr/local/lib/python3.10/dist-packages/torch/utils/data/_utils/collate.py", line 161, in collate_tensor_fn
#       out = elem.new(storage).resize_(len(batch), *list(elem.size()))
#   RuntimeError: Trying to resize storage that is not resizable
#
# The solution to this problem is to crop and/or resize input images to the
# same shape (and ensure that they're all 3 channels, which they should be!)
# before PyTorch attempts to batch them.
#
# So as a helpful convenience, we'll provide the problem-solving transforms as
# default arguments in this dataset module, rather than expecting the trainer
# code to know the specific PASCAL VOC-specific transforms that are needed.
#
# Also, Albumentations' `ToTensor` transform has been deprecated & removed.
# So now we use `ToTensorV2` instead:
#  https://albumentations.ai/docs/api_reference/pytorch/transforms/

#from torchvision import transforms
from torchvision.utils import make_grid

import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2

import conf
from cmdline import parse_list, parse_positive_int
from dataset_albument_vocseg import (PascalVOCSearchDataset,
        VOC_CLASSES, VOC_COLORMAP)


class PascalVocSegDataModule(pl.LightningDataModule):
    """A Lightning data-module for the PASCAL VOC segmentation dataset."""

    @staticmethod
    def add_dataset_specific_args(parser):
        # Configure the behaviour of the PASCAL VOC segmentation data-module.
        group = parser.add_argument_group('dataset',
                'Configure the PASCAL VOC segmentation data-module:')

        group.add_argument('--data-dir',        default=conf.data_dir,
                metavar='subdir')
        group.add_argument('--batch-size',      default=conf.batch_size,
                metavar='int', type=int)
        group.add_argument('--num-workers',     default=conf.num_workers,
                metavar='int', type=int)
        group.add_argument('--resize-image-H-W',default=conf.resize_image_H_W,
                action=parse_list(parse_positive_int, min_len=2, max_len=2),
                metavar='H,W')

        return parser


    def __init__(self, data_dir, *,
            batch_size, num_workers,
            resize_image_H_W,
            shuffle_train=True,
            prior_train_transforms=[],
            prior_testval_transforms=[],
            default_transforms=[
                    A.Normalize(
                            mean=[0.0, 0.0, 0.0],
                            std=[1.0, 1.0, 1.0],
                            max_pixel_value=255.0,
                    ),
                    ToTensorV2(transpose_mask=True),
            ]):
        super().__init__()
        self.data_dir = data_dir
        self.batch_size = batch_size
        self.num_workers = num_workers

        self.num_channels = 3  # RGB
        self.num_classes = len(VOC_CLASSES)

        (resize_height, resize_width) = resize_image_H_W
        resize_transform = A.Resize(height=resize_height, width=resize_width)
        self.train_transforms = A.Compose([
                        resize_transform,
                ] + prior_train_transforms + default_transforms)
        self.testval_transforms = A.Compose([
                        resize_transform,
                ] + prior_testval_transforms + default_transforms)

        self.shuffle_train = shuffle_train


    def prepare_data(self):
        # This method occurs on just a single node, before `setup` is called.

        # Download the "train/val" data in advance of setup.
        #
        # On the PASCAL VOC homepages:
        #  http://host.robots.ox.ac.uk/pascal/VOC/
        #  http://host.robots.ox.ac.uk/pascal/VOC/voc2012/
        # there is a mention of a "train/val" split in the dataset.
        #
        # If we inspect the PASCAL VOC 2012 dataset "VOCdevkit", particularly
        # in subdirectory `VOCdevkit/VOC2012/ImageSets/Segmentation/`, we may
        # observe that there are 3 text-files to list this "train/val" split:
        #  - `train.txt`
        #  - `trainval.txt`
        #  - `val.txt`
        #
        # We observe that `trainval.txt` appears to be the inter-mixture of
        # `train.txt` and `val.txt` (i.e., a union that has no intersection);
        # with `train.txt` consisting of the odd-line-numbered entries of
        # `trainval.txt`; and `val.txt` of the even-line-numbered entries.
        # [We observe that it's not an exact 50/50 split, because `train.txt`
        # and `val.txt` have different line-counts.]
        self.trainval_data_dir = os.path.join(self.data_dir, "trainval")
        PascalVOCSearchDataset(
                root=self.trainval_data_dir,
                image_set="trainval",
                download=True)

        # Download the "test" data in advance of setup.
        # 
        # The PASCAL VOC homepages explain that 2007 was
        # "the final year that annotation was released for the testing data":
        #  http://host.robots.ox.ac.uk/pascal/VOC/
        self.test_data_dir = os.path.join(self.data_dir, "test-2007")
        PascalVOCSearchDataset(
                root=self.test_data_dir,
                year="2007",
                image_set="test",
                download=True)


    def setup(self, stage):
        # This method is called after `prepare_data`.
        # This method might be distributed across multiple GPUs.
        self.train_ds = PascalVOCSearchDataset(
                root=self.trainval_data_dir,
                image_set="train",
                download=False,
                transform=self.train_transforms,
        )
        self.val_ds = PascalVOCSearchDataset(
                root=self.trainval_data_dir,
                image_set="val",
                download=False,
                transform=self.testval_transforms,
        )
        self.test_ds = PascalVOCSearchDataset(
                root=self.test_data_dir,
                year="2007",
                image_set="test",
                download=False,
                transform=self.testval_transforms,
        )


    def train_dataloader(self):
        return DataLoader(
                self.train_ds,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                shuffle=self.shuffle_train,
        )


    def val_dataloader(self):
        return DataLoader(
                self.val_ds,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                shuffle=False,
        )


    def test_dataloader(self):
        return DataLoader(
                self.test_ds,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                shuffle=False,
        )


class PascalVocSegPredRenderer:
    """A class that renders PASCAL VOC segmentation predictions as grid images."""

    def __init__(self):
        self.colour_map = torch.tensor(VOC_COLORMAP, dtype=torch_uint8)


    def __call__(self, images, targets, preds):
        assert images.dtype == torch_float32
        assert targets.dtype == torch_int64  # already `argmax`ed
        assert preds.dtype == torch_int64  # already `argmax`ed

        assert (images.min() >= 0.0)
        assert (images.max() <= 1.0)

        # The images dimensions should be: (batch, channels, height, width)
        # The targets dimensions should be: (batch, height, width)
        # The preds dimensions should be: (batch, height, width)
        assert images.ndim == 4
        assert targets.ndim == 3  # already `argmax`ed
        assert preds.ndim == 3  # already `argmax`ed
        (B, Chans, H, W) = images.shape
        assert targets.shape == (B, H, W)
        assert preds.shape == targets.shape

        # Convert images float32 -> uint8.
        # Be careful that we don't modify the supplied images argument.
        images_uint8 = images * 255.0
        images_uint8 = images_uint8.to(torch_uint8)
        #print("images_uint8.shape == %s" % str(images_uint8.shape))
        # => "images_uint8.shape == torch.Size([16, 3, 200, 200])"

        images_grid = make_grid(images_uint8)
        assert images_grid.dtype == torch_uint8
        #print("images_grid.shape == %s" % str(images_grid.shape))
        # => "images_grid.shape == torch.Size([3, 406, 1618])"

        targets_colour_mapped = self.colour_map[targets]
        assert targets_colour_mapped.dtype == torch_uint8
        #print("targets_colour_mapped.shape == %s" %
        #        str(targets_colour_mapped.shape))
        # => "targets_colour_mapped.shape == torch.Size([16, 200, 200, 3])"
        # NOTE: The shape is (16, 200, 200, 3); where 16 is the batch-size,
        # and (200, 200, 3) is supposed to be a 3-channel RGB image.
        # If this were Numpy, this would be perfect: (H, W, Chans).
        # But in Torch, the channel axis must be *before* the (H, W) axes:
        # a 3-channel RGB image must be (Chans, H, W).
        # So, we must move the channel axis to be *before* the (H, W) axes.
        targets_colour_mapped = torch_moveaxis(targets_colour_mapped, -1, 1)
        #print("targets_colour_mapped.shape == %s" %
        #        str(targets_colour_mapped.shape))
        # => "targets_colour_mapped.shape == torch.Size([16, 3, 200, 200])"
        assert targets_colour_mapped.shape == images_uint8.shape

        targets_grid = make_grid(targets_colour_mapped)
        assert targets_grid.dtype == torch_uint8
        #print("targets_grid.shape == %s" % str(targets_grid.shape))
        # => "targets_grid.shape == torch.Size([3, 406, 1618])"

        preds_colour_mapped = self.colour_map[preds]
        assert preds_colour_mapped.dtype == torch_uint8
        #print("preds_colour_mapped.shape == %s" %
        #        str(preds_colour_mapped.shape))
        # => "preds_colour_mapped.shape == torch.Size([16, 200, 200, 3])"
        # Likewise, we must move the channel axis to be before the (H, W) axes.
        preds_colour_mapped = torch_moveaxis(preds_colour_mapped, -1, 1)
        #print("preds_colour_mapped.shape == %s" %
        #        str(preds_colour_mapped.shape))
        # => "preds_colour_mapped.shape == torch.Size([16, 3, 200, 200])"
        assert preds_colour_mapped.shape == images_uint8.shape

        preds_grid = make_grid(preds_colour_mapped)
        assert preds_grid.dtype == torch_uint8
        #print("preds_grid.shape == %s" % str(preds_grid.shape))
        # => "preds_grid.shape == torch.Size([3, 406, 1618])"

        return [
                ('inputs',  images_grid),
                ('targets', targets_grid),
                ('preds',   preds_grid),
        ]
