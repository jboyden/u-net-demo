"""Torch submodules used by our main Lightning module.

Python classes (Torch modules) defined within this Python module:

- :class:`U_NetDoubleConv`

Copyright (c) 2023 James Boyden

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from torch import nn

# Submodule `argcheck` provides automated checking of function-call argument
# types & values via the `@validate_call` decorator.
#
# The constraints upon argument types/values are specified per-parameter
# using PEP-484 type hint annotations (``parameter_name: type_annotation``),
# extended using the PEP-593 ``Annotated[T, metadata]`` type annotation
# for value constraints as metadata.
#
# If an argument check is violated when the decorated function is called,
# an exception of type `argcheck.ArgCheckException` will be raised.
# For more details, consult the docstrings in the `argcheck` submodule.
from argcheck import Annotated, isPositive, validate_call


class U_NetDoubleConv(nn.Module):
    """This Torch module implements a pair of adjacent convolutions in a U-Net.

    The best way to understand the purpose of this Torch module is to examine
    the U-Net architecture diagram, Figure 1 in the original U-Net paper:

    - The U-Net is a chain of Torch modules linked together into a 'U' shape:
      a "contracting path" on the left; and an "expanding path" on the right.

    - The 'U' shape is formed from repeated chaining of the following pattern:
      1. a 3x3 convolution (unpadded)
      2. a ReLU
      3. a 3x3 convolution (unpadded)

    - This repeating pattern appears in the architecture diagram as
      a pair of right-pointing dark-blue arrows representing the convolutions
      (which connect light-blue boxes representing the feature maps).

    - This Torch module (this Python class) implements this repeating pattern.

    - On the "contracting path" (on the left of the 'U' shape), instances
      of the repeating pattern are linked by a 2x2 max-pooling operation
      with stride 2 for downsampling.

    - On the "expanding path" (on the right of the 'U' shape), instances
      of the repeating pattern are linked by 2x2 transposed convolutions
      for upsampling.

    In our implementation of the repeating pattern in this Torch module,
    we make 2 significant differences from the original U-Net paper:  

    1. Rather than the 3x3 "valid convolutions" that shave 1px off the border
      of the convolution result (resulting in a progressive reduction of the
      (height, width) resolution of each successive feature map), we instead
      use 1px-padded 3x3 "same convolutions" that maintain the resolution.
      This avoids the need to crop "skip connections" before concatenation;
      and also avoids the need to tile multiple less-than-input-image-size
      segmentation results.

    2. We apply batch normalisation after the 3x3 convolution, before the ReLU.
      [And then because of the batch normalisation step, there's no benefit
      to adding a bias after the convolution step, because any bias would be
      negated by the batch normalisation anyway.  So we specify ``bias=False``
      to the ``nn.Conv2d`` constructor.]

    Observe that this Torch module contains only a single data attribute:
    
    :ivar conv: (of type ``nn.Sequential``)
    
    So this class definition could be replaced by a single Python module-level
    definition::

        U_NetDoubleConv = nn.Sequential(...)

    But then we couldn't include this charming, informative docstring!
    """
    @validate_call
    def __init__(self,
            in_channels: Annotated[int, isPositive],
            out_channels: Annotated[int, isPositive],
    ):
        super().__init__()
        self.conv = nn.Sequential(
                # https://pytorch.org/docs/stable/generated/torch.nn.Conv2d.html
                #
                #       torch.nn.Conv2d(
                #               in_channels, out_channels, kernel_size,
                #               stride=1, padding=0, dilation=1, groups=1,
                #               bias=True,
                #               padding_mode='zeros',
                #               device=None, dtype=None)
                #
                # So the following call is equivalent to:
                #       torch.nn.Conv2d(
                #               in_channels,
                #               out_channels,
                #               kernel_size=3,
                #               stride=1,
                #               padding=1,
                #               bias=False)
                #
                # Also, we set `bias=False` because we also use BatchNorm;
                # and BatchNorm will cancel-out any bias.
                nn.Conv2d(in_channels, out_channels, 3,
                        stride=1, padding=1, bias=False),

                # https://pytorch.org/docs/stable/generated/torch.nn.BatchNorm2d.html
                nn.BatchNorm2d(out_channels),

                # https://pytorch.org/docs/stable/generated/torch.nn.ReLU.html
                nn.ReLU(inplace=True),

                # And again...
                nn.Conv2d(out_channels, out_channels, 3,
                        stride=1, padding=1, bias=False),
                nn.BatchNorm2d(out_channels),
                nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.conv(x)
