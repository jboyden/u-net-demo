"""A `main` function to train a `U_Net` using PASCAL VOC segmentation dataset.

Copyright (c) 2023 James Boyden

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import datetime

import torch
import pytorch_lightning as pl
import albumentations as A

import conf

from pytorch_lightning.loggers import TensorBoardLogger

from cmdline import get_argument_parser
from dataset import PascalVocSegDataModule, PascalVocSegPredRenderer
from model import U_Net


def main():
    # Create and populate the command-line argument parser.
    parser = get_argument_parser(
            descr='Train a U-Net model using PASCAL VOC segmentation dataset')

    PascalVocSegDataModule.add_dataset_specific_args(parser)
    U_Net.add_model_specific_args(parser)
    _add_trainer_specific_args(parser)

    # Extract (parse) the command-line arguments from the parser.
    parsed_args = parser.parse_args()

    # Different behaviours for "short" runs vs overfitting vs "full" runs.
    if parsed_args.do_short_run:
        experiment_name = "%s_short" % parsed_args.experiment_name
        # Because we *want* to overfit, don't shuffle the training images.
        shuffle_train = False
        # Because we *want* to overfit, don't transform the training images.
        train_transforms = []
    elif parsed_args.do_overfit_batches:
        experiment_name = "%s_overfit" % parsed_args.experiment_name
        # Because we *want* to overfit, don't shuffle the training images.
        shuffle_train = False
        # Because we *want* to overfit, don't transform the training images.
        train_transforms = []
    else:
        # It's a full run over the entire training set.
        experiment_name = parsed_args.experiment_name
        # Randomly shuffle the training images.
        shuffle_train = True
        # Randomly transform each training image, each time it's used:
        #  - 100% of the time, rotate the image by an amount up to 30 degrees
        #  - 50% of the time, flip the image horizontally (to preserve horizon)
        #
        # As the original U-Net paper notes:
        #   """
        #   As for our tasks there is very little training data available,
        #   we use excessive data augmentation by applying elastic deformations
        #   to the available training images.
        #   """
        train_transforms = [
                A.Rotate(limit=30, p=1.0),
                A.HorizontalFlip(p=0.5),
        ]

    # According to the PyTorch-Lightning docs:
    #  https://lightning.ai/docs/pytorch/stable/extensions/logging.html#control-logging-frequency
    #   """
    #   It may slow down training to log on every single batch.
    #   By default, Lightning logs every 50 rows, or 50 training steps.
    #   To change this behaviour, set the `log_every_n_steps` `Trainer` flag.
    #   """
    #
    # Note that (currently?) the flag `log_every_n_steps` only really controls
    # logging during training:
    #  https://github.com/Lightning-AI/lightning/issues/10436
    #   """
    #   carmocca commented on Nov 10, 2021
    #
    #   The huge issue today with `log_every_n_steps` is that with high
    #   probability, it is broken for Trainer.validate, Trainer.test, and
    #   Trainer.predict.
    #   <snip>
    #
    #   Therefore, if your LightningModule calls `self.log` inside of validate,
    #   test, or predict steps, it is highly likely that your data will not be
    #   logged! You would have to get lucky that the global step is a multiple
    #   of `log_every_n_steps` from a prior fitting routine. It is not obvious
    #   at all to an end user why calling `self.log` results in no data
    #   being updated. This has also been a consistent complaint amongst users
    #   of Lightning that I'm aware of, especially when trying to log metrics
    #   with Trainer.test.
    #
    #   The workaround we have is to set `Trainer(log_every_n_steps=1)` and
    #   gate `LightningModule.log` with another flag for the log frequency.
    #   This way, users can at least control the granularity with which
    #   they want to log without any surprising behavior from the Lightning
    #   framework interfering with this.
    #   """
    #
    # Hence, we introduce our own logging flag that counts in training epochs.
    if (parsed_args.log_images_every_n_epochs is not None and
            parsed_args.log_images_every_n_epochs > 0):
        # The caller has chosen to override the default.
        log_images_every_n_epochs = parsed_args.log_images_every_n_epochs
    elif parsed_args.do_short_run:
        # We use a different default in short runs.
        # By default, `(conf.num_epochs_short == 20)`;
        # in which case, we will log images every 2 epochs.
        log_images_every_n_epochs = max(parsed_args.num_epochs_short // 10, 1)
    elif parsed_args.do_overfit_batches:
        # We use a different default when overfitting batches.
        log_images_every_n_epochs = 20
    else:
        # It's a full run over the entire training set.
        log_images_every_n_epochs = conf.log_images_every_n_epochs

    # Instantiate classes, making use of specified command-line arguments.
    data = PascalVocSegDataModule(
            data_dir=parsed_args.data_dir,
            batch_size=parsed_args.batch_size,
            num_workers=parsed_args.num_workers,
            resize_image_H_W=parsed_args.resize_image_H_W,
            shuffle_train=shuffle_train,
            prior_train_transforms=train_transforms,
    )
    # Note: The `U_Net` hyperparameters `in_channels` & `out_channels`
    # must correspond to the number of image channels in the dataset's
    # training images, and the number of classes in the dataset
    # (because that's what the model will be trained upon).
    #
    # So there's no point allowing the caller to specify
    # these hyperparameters as command-line arguments.
    model = U_Net(
            in_channels=data.num_channels,
            out_channels=data.num_classes,
            learning_rate=parsed_args.learning_rate,
    )
    if parsed_args.log_progress_images:
        model.log_images_every_n_epochs = log_images_every_n_epochs
        model.prediction_renderer = PascalVocSegPredRenderer()

    logger = TensorBoardLogger(
            save_dir=parsed_args.logger_dirname,
            name=experiment_name)

    if parsed_args.do_short_run:
        # Do a short run, as a quick sanity check.
        # We also overfit batches on short runs.
        trainer = pl.Trainer(
                logger=logger,
                min_epochs=parsed_args.min_epochs,
                max_epochs=parsed_args.num_epochs_short,
                overfit_batches=parsed_args.num_overfit_batches,
                # By default, `(num_epochs_short == 20)`;
                # in which case, we will log every 2 training steps.
                log_every_n_steps=max(parsed_args.num_epochs_short // 20, 2),
        )
    elif parsed_args.do_overfit_batches:
        # Overfit batches.
        trainer = pl.Trainer(
                logger=logger,
                min_epochs=parsed_args.min_epochs,
                max_epochs=parsed_args.num_epochs,
                overfit_batches=parsed_args.num_overfit_batches,
        )
    else:
        # Don't overfit batches; train on ALL batches.
        trainer = pl.Trainer(
                logger=logger,
                min_epochs=parsed_args.min_epochs,
                max_epochs=parsed_args.num_epochs,
        )
    trainer.fit(model, data)


def _add_trainer_specific_args(parser):
    # Configure the trainer.
    group = parser.add_argument_group('train',
            'Configure the PyTorch-Lightning trainer:')

    # Training duration
    group.add_argument('--min-epochs',          default=conf.min_epochs,
            metavar='int', type=int)
    group.add_argument('--num-epochs',          default=conf.num_epochs,
            metavar='int', type=int)

    # Overfit batches
    group.add_argument('--num-overfit-batches', default=conf.num_overfit_batches,
            metavar='int', type=int)
    group.add_argument('--do-overfit-batches',  default=conf.do_overfit_batches,
            action='store_true')

    # Short run
    group.add_argument('--num-epochs-short',    default=conf.num_epochs_short,
            metavar='int', type=int)
    group.add_argument('--do-short-run',        default=conf.do_short_run,
            action='store_true')

    # Logging
    group.add_argument('--logger-dirname',      default=conf.logger_dirname,
            metavar='subdir')
    group.add_argument('--experiment-name',     default=conf.experiment_name,
            metavar='valid_ident')

    # Default to `None` (rather than an `int`) so we can tell whether the
    # caller has chosen to override the default with an intentional value.
    # (We use a different default in short runs.)
    group.add_argument('--log-images-every-n-epochs',   default=None,
            metavar='int', type=int)

    # Log progress images, in a per-batch image-grid of inputs & predictions
    # that can be viewed in TensorBoard.
    group.add_argument('--log-progress-images', default=conf.log_progress_images,
            action='store_true')

    return parser


if __name__ == "__main__":
    start = datetime.datetime.now()
    # Class `datetime.datetime` has a convenient `__str__` (for "%s"):
    #   "2023-10-06 14:02:04.682074"
    print("Start training: %s" % start)

    # Train our model.
    main()

    finish = datetime.datetime.now()
    print("Finish training: %s" % finish)
    duration = finish - start
    # Class `datetime.timedelta` also has a convenient `__str__` (for "%s"):
    #   "0:00:13.232875"
    print("Training duration: %s" % duration)
