U-Net Demo
==========

My own
[PyTorch-Lightning](https://pypi.org/project/pytorch-lightning/)
implementation of a
[U-Net model Convolutional Neural Network](https://lmb.informatik.uni-freiburg.de/people/ronneber/u-net/)
for image segmentation, including a Python script to train on the
[PASCAL VOC segmentation dataset](http://host.robots.ox.ac.uk/pascal/VOC/).

Code: files in subdirectory `u_net/`
------------------------------------

The code of my U-Net model implementation is in subdirectory `u_net/`:

- [`argcheck/`](https://gitlab.com/jboyden/u-net-demo/-/tree/main/u_net/argcheck):
automated checking of function-call argument types
- [`cmdline.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/cmdline.py):
functions to parse command-line arguments, for use with Python `argparse`
- [`conf.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/conf.py):
hard-coded default configuration options (override by command-line args)
- [`dataset.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/dataset.py):
a data-module to access the PASCAL VOC segmentation dataset
- [`dataset_albument_vocseg.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/dataset_albument_vocseg.py): convenient enumerations of VOC classes and the VOC colormap
- [`model.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/model.py):
a class `U_Net` that implements a U-Net model for image segmentation
- [`submodules.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/submodules.py):
submodules used by our main `U_Net` model
- [`train.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/train.py):
a `main` function to train a `U_Net` using PASCAL VOC segmentation dataset.

All of this is entirely my own code,
except for file `u_net/dataset_albument_vocseg.py`,
which is substantially based on
[MIT-Licensed code from Albumentations.ai](https://github.com/albumentations-team/autoalbument/blob/master/examples/pascal_voc/dataset.py),
which provides convenient enumerations of VOC classes and the VOC colormap.

I'm continuing to add functionality for convenient testing on user-specified
images and a Flask API.

Code: subdirectory `u_net/argcheck/`
------------------------------------

Provides a Python3 decorator (`@validate_call`) for automated checking of
function-call argument types/values using Python3 stdlib
[`inspect`](https://docs.python.org/3/library/inspect.html) module.

Use of `argcheck` enables you to remove verbose, repetitive argument-checking
boilerplate code from within the function, by instead annotating per-parameter
constraints.

In this case,
using `argcheck` enables the `U_Net.__init__()` method to be declared as:

```python
@validate_call
def __init__(self,
        in_channels: Annotated[int, isPositive]=3,
        out_channels: Annotated[int, isPositive]=2,
        num_features_per_scale: Annotated[
                Sequence[int],
                [isNotEmpty, isMonotonicIncr, eachAll(isPositive)]
        ]=[64, 128, 256, 512, 1024],
        *,
        learning_rate: Annotated[float, isPositive]=1e-2,
        multi_loss_fn=nn.CrossEntropyLoss,
        binary_loss_fn=nn.BCEWithLogitsLoss,
):
```

Official `argcheck-python3` project Git repository with LICENSE and tests:
 https://github.com/jboy/argcheck-python3

The U-Net model architecture
----------------------------

The article citation specified on the
[U-Net model website](https://lmb.informatik.uni-freiburg.de/people/ronneber/u-net/) is:

[**U-Net: Convolutional Networks for Biomedical Image Segmentation**](https://lmb.informatik.uni-freiburg.de/Publications/2015/RFB15a/)\
Paper authors: **Olaf Ronneberger**, **Philipp Fischer**, **Thomas Brox**\
Medical Image Computing and Computer-Assisted Intervention (MICCAI),
Springer, LNCS, Vol.9351: 234--241, 2015,\
available at
[**arXiv:1505.04597**](https://arxiv.org/abs/1505.04597)

![The U-Net model architecture](https://gitlab.com/jboyden/u-net-demo/-/raw/main/u-net-architecture.png "The U-Net model architecture")

My U-Net model implementation
-----------------------------

My U-Net model implementation is in Python class
[`u_net.model.U_Net`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/model.py),
a PyTorch-Lightning
[`LightningModule`](https://lightning.ai/docs/pytorch/stable/common/lightning_module.html).

In this U-Net implementation, I made 5 note-worthy deviations from the
[original U-Net paper](https://lmb.informatik.uni-freiburg.de/people/ronneber/u-net/):

1. In contrast to the fixed input resolution of 572 x 572 pixels specified
in the original U-Net paper, this implementation accepts images of an
arbitrary `height` x `width` input resolution.

2. In contrast to the fixed sequence of num-features-per-level specified
in the original U-Net paper (specifically: `[64, 128, 256, 512, 1024]`),
this implementation allows client code to specify any number of levels,
and any number of features per level.

3. In submodule `U_NetDoubleConv`:
Rather than the 3x3 "valid convolutions" that shave 1px off the border
of the convolution result (resulting in a progressive reduction of the
(height, width) resolution of each successive feature map), I instead
used 1px-padded 3x3 "same convolutions" that maintain the resolution.
This avoids the need to crop "skip connections" before concatenation;
and also avoids the need to tile multiple less-than-input-image-size
segmentation results.

4. In submodule `U_NetDoubleConv`:
I apply batch normalisation after the 3x3 convolution, before the ReLU.
(And then because of the batch normalisation step, there's no benefit
to adding a bias after the convolution step, because any bias should
theoretically be negated by the batch normalisation anyway.
So I specify `bias=False` to the `nn.Conv2d` constructor.)

5. Rather than exactly 2 channels of output by the final convolution layer
(for a per-pixel multi-class classifier of exactly 2 classes), the final
convolution layer in this implementation allows any positive number of
output channels (for any number of output classes), even including just
1 channel (for a per-pixel binary classifier).

Creating a new U-Net model instance
-----------------------------------

To use this U-Net model, you must create an instance of the Python class
[`u_net.model.U_Net`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/model.py).

The `U_Net.__init__()` method takes 3 optional positional arguments
to specify the structure of the model:

- `in_channels` (positive int):
defaults to `3`, expecting an RGB image
- `out_channels` (positive int):
defaults to `2`, for 2-class multi-class classification (like the U-Net paper)
- `num_features_per_scale` (non-empty sequence of positive int):
defaults to `[64, 128, 256, 512, 1024]` (like the U-Net paper)

The `U_Net.__init__()` method also takes 3 optional keyword-only arguments
to specify the loss function or configure the optimizer:

- `learning_rate` (float):
defaults to `1.0e-2`
- `multi_loss_fn` (loss function to use for multi-class):
defaults to
[`nn.CrossEntropyLoss`](https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html)
(like the U-Net paper)
- `binary_loss_fn` (loss function to use for binary-class):
defaults to
[`nn.BCEWithLogitsLoss`](https://pytorch.org/docs/stable/generated/torch.nn.BCEWithLogitsLoss.html)

These arguments are all optional because they have reasonable default values.

You need to train the model before use
--------------------------------------

When you create an instance of Python class
[`u_net.model.U_Net`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/model.py),
the U-Net model is **untrained**,
full of uninitialised (effectively random) neural network parameters.

Before you can use this U-Net model for inference
(to predict image segmentations for supplied input images),
**you must train the model** on an image segmentation dataset (such as the
[PASCAL VOC segmentation dataset](http://host.robots.ox.ac.uk/pascal/VOC/)).

Fortunately, this Git repository also includes:

- in [`u_net/dataset.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/dataset.py):
a data-module to encapsulate the
[TorchVision dataset for PASCAL VOC segmentation](https://pytorch.org/vision/main/generated/torchvision.datasets.VOCSegmentation.html)
for training using PyTorch-Lightning;
- in [`u_net/train.py`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/u_net/train.py):
a Python script to train this U-Net model from
the Linux command-line using PyTorch-Lightning.

(To share credit where it's due,
[PyTorch-Lightning](https://pypi.org/project/pytorch-lightning/)
was created specifically to make it easier to train PyTorch models...)

Python package dependencies to train on Linux
---------------------------------------------

If you have a fresh Ubuntu Linux system, you must install
these Python packages to be able to train the network:

- `torch`
- `torchvision`
- `pytorch-lightning`
- `torchmetrics`
- `tensorboard`
- `six`
- `albumentations`

These Python packages are listed in the file
[`requirements-train-deps.txt`](https://gitlab.com/jboyden/u-net-demo/-/blob/main/requirements-train-deps.txt).

Recommended steps to install dependencies
-----------------------------------------

If you have a fresh Ubuntu Linux system, I recommend the following steps
to install dependencies and prepare your system to train the U-Net:

1. Ensure that Python3, Python3-pip, and Python3-venv are installed
(along with a few other convenient utilities):
```
sudo apt update
sudo apt install python3 python3-pip python3-venv bzip2 curl make screen w3m
```
2. Create a subdirectory for our U-Net training:
```
mkdir u-net && cd u-net
```
3. **If we're on a remote Linux machine,** start a
[`screen`](https://wiki.archlinux.org/title/GNU_Screen) session,
so that our environment (and any long-running U-Net training process
running within it) will persist if the SSH connection is broken:
```
screen
```
4. Use the system `pip` to bootstrap a user installation of `pip`,
to ensure we have the latest version of `pip`  installed.
```
python3 -m pip --version
python3 -m pip install --user --upgrade pip
python3 -m pip --version
```
5. Setup a Python3 "venv" (Python3-stdlib virtual environment):
```
python3 -m venv env
ls -l | grep env
```
6. Activate the new virtual environment:
```
which python3
source env/bin/activate
which python3
```
7. Download the `u-net-demo` source code from GitLab in `tar.bz2` format:
```
curl --remote-name https://gitlab.com/jboyden/u-net-demo/-/archive/main/u-net-demo-main.tar.bz2
```
8. Untar the `u-net-demo` source code:
```
tar jxvf u-net-demo-main.tar.bz2
```
9. Install the Python package dependencies:
```
python3 -m pip install -r u-net-demo-main/requirements-train-deps.txt
```

Command-line arguments for training
-----------------------------------

When you run the command `python3 u_net/train.py --help`, it provides the
following help/usage message about the command-line arguments for training:

```
usage: train.py [-h] [--data-dir subdir] [--batch-size int]
                [--num-workers int] [--resize-image-H-W H,W]
                [--num-features-per-scale int,int,...] [--learning-rate float]
                [--min-epochs int] [--num-epochs int]
                [--num-overfit-batches int] [--do-overfit-batches]
                [--num-epochs-short int] [--do-short-run]
                [--logger-dirname subdir] [--experiment-name valid_ident]
                [--log-images-every-n-epochs int] [--log-progress-images]

Train a U-Net model using PASCAL VOC segmentation dataset

options:
  -h, --help            show this help message and exit

dataset:
  Configure the PASCAL VOC segmentation data-module:

  --data-dir subdir
  --batch-size int
  --num-workers int
  --resize-image-H-W H,W

model:
  Hyperparameters for the U-Net model:

  --num-features-per-scale int,int,...
  --learning-rate float

train:
  Configure the PyTorch-Lightning trainer:

  --min-epochs int
  --num-epochs int
  --num-overfit-batches int
  --do-overfit-batches
  --num-epochs-short int
  --do-short-run
  --logger-dirname subdir
  --experiment-name valid_ident
  --log-images-every-n-epochs int
  --log-progress-images

Copyright (C) 2023 James Boyden: https://gitlab.com/jboyden/u-net-demo/
```
